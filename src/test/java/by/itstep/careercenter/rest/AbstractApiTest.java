package by.itstep.careercenter.rest;

import by.itstep.careercenter.constants.CareerCenterConstants;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.http.Method;
import io.restassured.specification.RequestSpecification;
import org.testng.annotations.BeforeMethod;

import static by.itstep.careercenter.constants.CareerCenterConstants.ADMIN_LOGIN;
import static by.itstep.careercenter.constants.CareerCenterConstants.ADMIN_PASSWORD;

public abstract class AbstractApiTest {

    @BeforeMethod
    public void setUp() {
        RestAssured.baseURI = CareerCenterConstants.HOST;
    }

    protected String getToken() {
        RequestSpecification httpRequest = RestAssured.given();
        httpRequest.contentType(ContentType.JSON);
        httpRequest.body("{\n" +
                "  \"password\": \"" + ADMIN_PASSWORD + "\",\n" +
                "  \"username\": \"" + ADMIN_LOGIN + "\"\n" +
                "}");
        return httpRequest
                        .request(Method.POST, "/api/v1/auth/login")
                        .body()
                        .jsonPath()
                        .getString("token");

    }

    protected RequestSpecification getSpecificationWithToken() {
        String token = getToken();

        RequestSpecification httpRequest = RestAssured.given();
        httpRequest.header("Authorization","Bearer_" + token);

        return httpRequest;
    }
}
